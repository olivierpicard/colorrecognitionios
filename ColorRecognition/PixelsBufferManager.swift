//
//  PixelsBufferManager.swift
//  ColorRecognition
//
//  Created by Olivier Picard on 23/12/2019.
//  Copyright © 2019 Olivier Picard. All rights reserved.
//

import Foundation
import AVFoundation

typealias PixelInt = (r: UInt8, g: UInt8, b: UInt8)

class PixelsBufferManager
{
    private let modelDataHandled: ModelDataHandler!
    
    
    init()
    {
        modelDataHandled = ModelDataHandler()
//        let result = self.modelDataHandled.runModel(rgbArray: [1.0, 1, 0.0])
//        print(result![0].confidence, result![0].label, result![0].indice, separator: ", ")
    }
    
    
    func predict(buffer: CVPixelBuffer!) -> String
    {
        let width = CVPixelBufferGetWidth(buffer)
        let height = CVPixelBufferGetHeight(buffer)
        let pixels: [PixelInt] = areaPixels(
            center: CGPoint(x: width/2, y: height/2),
            areaSize: CGSize(width: 100, height: 100),
            buffer: buffer
        )
        let meanPixel = meanPixels(pixelArray: pixels)
        let floatPixel: [Float32] = pixelToFloatArray(pixel: meanPixel)
        
        assert(floatPixel.count == 3)
        
        let result = modelDataHandled.runModel(rgbArray: floatPixel)
        print(result![0].confidence, result![0].label, result![0].indice, separator: ", ")
        return result![0].label
    }
    
    
    func pixelToFloatArray(pixel: PixelInt) -> [Float32]
    {
        return [Float32(pixel.r) / 255.0,
                Float32(pixel.g) / 255.0,
                Float32(pixel.g) / 255.0,]
    }
    
    
    func meanPixels(pixelArray: [PixelInt]) -> PixelInt
    {
        let pixelCount = pixelArray.count
        var summedPixel = (r: 0, g: 0, b: 0)
        
        for pixel in pixelArray {
            summedPixel.r += Int(pixel.r)
            summedPixel.g += Int(pixel.g)
            summedPixel.b += Int(pixel.b)
        }
        
        return PixelInt(r: UInt8(summedPixel.r / pixelCount),
                        g: UInt8(summedPixel.g / pixelCount),
                        b: UInt8(summedPixel.b / pixelCount))
    }
    
    
    func areaPixels(center: CGPoint, areaSize: CGSize,
                    buffer: CVPixelBuffer) -> [PixelInt]
    {
        let minBoundX = Int(center.x - areaSize.width / 2)
        let maxBoundX = Int(center.x + areaSize.width / 2)
        let minBoundY = Int(center.y - areaSize.height / 2)
        let maxBoundY = Int(center.y + areaSize.height / 2)
        var pixelArray: [PixelInt] = []
        
        for col in minBoundX...maxBoundX {
            for row in minBoundY...maxBoundY {
                pixelArray.append(
                    getPixel(x: col, y: row, movieFrame: buffer)
                )
            }
        }
        
        return pixelArray
    }
    
    
    func getPixel(x: Int, y: Int, movieFrame: CVPixelBuffer) -> PixelInt
    {
        CVPixelBufferLockBaseAddress(movieFrame, .readOnly)
        defer { CVPixelBufferUnlockBaseAddress(movieFrame, .readOnly) }
    
        let baseAddress = CVPixelBufferGetBaseAddress(movieFrame)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(movieFrame)
        let buffer = baseAddress!.assumingMemoryBound(to: UInt8.self)

        let index = x*4 + y*bytesPerRow
        let b = buffer[index]
        let g = buffer[index+1]
        let r = buffer[index+2]

        return (r, g, b)
    }
    
    
    
    
}
