//
//  ModelDataHandler.swift
//  ColorRecognition
//
//  Created by Olivier Picard on 22/12/2019.
//  Copyright © 2019 Olivier Picard. All rights reserved.
//

import Foundation
import TensorFlowLite

typealias FileInfo = (name: String, extension: String)

struct Inference {
    let confidence: Float
    let label: String
    let indice: Int
}

enum ModelDataHandlerError: Error {
    case NotFoundClassNamesFile
}


class ModelDataHandler
{
    private var interpreter: Interpreter!
    private var className: [String]!
    private let modelFile: FileInfo = (name: "advance_color_tensorflowlite",
                                       extension: "tflite")
    
    
    init?(threadCount: Int = 1)
    {
        guard let modelPath = Bundle.main.path(
            forResource: modelFile.name,
            ofType: modelFile.extension)
            else {
                print("Failed to load the model file : \(modelFile.name)")
                return nil
        }

        var options = InterpreterOptions()
        options.threadCount = threadCount
        
        do {
            interpreter = try Interpreter(
                modelPath: modelPath,
                options: options)
        } catch let error {
            print("Failed to start Interpreter :",
                  "\(error.localizedDescription)")
            return nil
        }
        
        do { className = try getClassNames() }
        catch let error {
            print("Failed to load class names text file :",
                  "\(error.localizedDescription)")
            return nil
        }
    }
    
    
    func runModel(rgbArray: [Float32]) -> [Inference]?
    {
        let outputTensor: Tensor
        
        do {
            try interpreter.allocateTensors()
            let rgbData = Data(
                buffer: UnsafeBufferPointer(
                    start: rgbArray, count: rgbArray.count))
            
            try interpreter.copy(rgbData, toInputAt: 0)
            try interpreter.invoke()
            outputTensor = try interpreter.output(at: 0)
        } catch let error {
            print("Failed to run the model. An error occurred :",
                "\(error.localizedDescription)")
            return nil
        }
        
        let results: [Float32] = [Float32](unsafeData: outputTensor.data) ?? []
        return getTopN(results: results)
    }
    
    
    func getTopN(results: [Float32], N: Int = 1) -> [Inference]
    {
        let pairedResults = zip(className.indices, results)
        let sortedResults = pairedResults.sorted { $0.1 > $1.1 }.prefix(N)
        return sortedResults.map {
            result in Inference(
                confidence: result.1,
                label: className[result.0+1],
                indice: result.0+1
            )
        }
    }
    
    
    func getClassNames() throws -> [String]
    {
        guard let classNamePath = Bundle.main.path(
            forResource: "labels",
            ofType: "txt") else {
                throw ModelDataHandlerError.NotFoundClassNamesFile
        }
        let text = try String(contentsOfFile: classNamePath,
                              encoding: .utf8)
        
        let array = text.components(separatedBy: "\r\n")
        return array
    }
    
}

// MARK: Extensions
extension Array {
  /// Creates a new array from the bytes of the given unsafe data.
  ///
  /// - Warning: The array's `Element` type must be trivial in that it can be copied bit for bit
  ///     with no indirection or reference-counting operations; otherwise, copying the raw bytes in
  ///     the `unsafeData`'s buffer to a new array returns an unsafe copy.
  /// - Note: Returns `nil` if `unsafeData.count` is not a multiple of
  ///     `MemoryLayout<Element>.stride`.
  /// - Parameter unsafeData: The data containing the bytes to turn into an array.
  init?(unsafeData: Data) {
    guard unsafeData.count % MemoryLayout<Element>.stride == 0 else { return nil }
    #if swift(>=5.0)
    self = unsafeData.withUnsafeBytes { .init($0.bindMemory(to: Element.self)) }
    #else
    self = unsafeData.withUnsafeBytes {
      .init(UnsafeBufferPointer<Element>(
        start: $0,
        count: unsafeData.count / MemoryLayout<Element>.stride
      ))
    }
    #endif  // swift(>=5.0)
  }
}
