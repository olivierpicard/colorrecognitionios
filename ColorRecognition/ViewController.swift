//
//  ViewController.swift
//  ColorRecognition
//
//  Created by Olivier Picard on 23/12/2019.
//  Copyright © 2019 Olivier Picard. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    @IBOutlet weak var label_colorName: UILabel!
    var inputDevice: AVCaptureDeviceInput!
    let captureSession = AVCaptureSession()
    var device: AVCaptureDevice!
    let videoDataOutput = AVCaptureVideoDataOutput()
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer!
    let pixelBufferManager = PixelsBufferManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCaptureSession()
        setupDevice()
        setupInput()
        setupOutput()
        setupPreviewLayer()
        startCaptureSession()
    }
    
    
    func setupCaptureSession()
    {
       captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }


    func setupDevice()
    {
        device = AVCaptureDevice.default(.builtInWideAngleCamera,
                                            for: .video, position: .back)
    }

    
    func setupInput()
    {
        guard let inputDevice = try? AVCaptureDeviceInput(device: device!),
            captureSession.canAddInput(inputDevice)
            else {
                print("Failed to add input device")
                return
        }
        self.inputDevice = inputDevice
        captureSession.addInput(self.inputDevice)
    }

    
    func setupOutput()
    {
        let sampleBufferQueue = DispatchQueue(label: "sampleBufferQueue")
        videoDataOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)
        videoDataOutput.alwaysDiscardsLateVideoFrames = true
        videoDataOutput.videoSettings = [ String(kCVPixelBufferPixelFormatTypeKey) : kCMPixelFormat_32BGRA]

        guard captureSession.canAddOutput(videoDataOutput) else { return }
        captureSession.sessionPreset = .photo
        captureSession.addOutput(videoDataOutput)
        captureSession.commitConfiguration()
    }


    func setupPreviewLayer()
    {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer, at: 0)
    }
    
    func startCaptureSession()
    {
        captureSession.startRunning()
    }
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection)
    {
        // Converts the CMSampleBuffer to a CVPixelBuffer.
        let pixelBuffer: CVPixelBuffer? = CMSampleBufferGetImageBuffer(sampleBuffer)
        guard let imagePixelBuffer = pixelBuffer else {
            return
        }
        let result = pixelBufferManager.predict(buffer: imagePixelBuffer)
        
        DispatchQueue.main.async {
            self.label_colorName.text = result
        }
    }
    
}

